// GOI API:
const BASE_URL = "https://62b07883196a9e98702448f3.mockapi.io/mon-an";
export let servicesMonAn = {
    layDanhSachMonAn: () => {
        return axios({
            url: "https://62b07883196a9e98702448f3.mockapi.io/mon-an",
            method: "GET",
        });
    },
    xoaMonAn: (id) => {
        return axios({
            url: `https://62b07883196a9e98702448f3.mockapi.io/mon-an/${id}`,
            method: "DELETE",
        });
    },
    themMoiMonAn: (monAn) => {
        return axios({
            url: "https://62b07883196a9e98702448f3.mockapi.io/mon-an",
            method: "POST",
            data: monAn
        });
    },
    layChiTietMonAn: (idMonAn) => {
        return axios({
            url: `https://62b07883196a9e98702448f3.mockapi.io/mon-an/${idMonAn}`,
            method: "GET",
        });
    },
    capNhatMon: (monAn) => {
        return axios({
            url: `${BASE_URL}/${monAn.id}`,
            method: "PUT",
            data: monAn
        });
    }
};

