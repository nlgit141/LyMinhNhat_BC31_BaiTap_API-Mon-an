export let monAnController = {
    layThongTinMonAnTuForm: () => {
        let tenMonAn = document.getElementById("ten-monan").value;
        let giaMonAn = document.getElementById("gia-monan").value;
        let moTa = document.getElementById("mota-monan").value;

        let monAn = {
            name: tenMonAn,
            price: giaMonAn,
            description: moTa
        };
        return monAn;
    },
    showThongTinlenForm: (monAn) => {
        document.getElementById("ten-monan").value = monAn.name;
        document.getElementById("gia-monan").value = monAn.price;
        document.getElementById("mota-monan").value = monAn.description;
    }
};