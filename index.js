
let listFood = [];
let idFoodEdited = null;

// RENDER FOOD RA GIAO DIỆN
let renderListFood = (list) => {
    let contentHTML = "";
    for (let index = 0; index < list.length; index++) {
        let monAn = list[index];
        let contentFood = `
        <tr class='table'>
            <td>${monAn.id}</td>
            <td>${monAn.name}</td>
            <td>${monAn.price}</td>
            <td>${monAn.description}</td>
            <td class='d-flex justify-content-end border-bottom border-light'>
                <button onclick="xoaMonAn(${monAn.id})" class='btn btn-danger'>xóa</button>
                <button onclick="suaMonAn(${monAn.id})" class='btn btn-warning ml-1'>sửa</button>
            </td>
        </tr>
        `;
        contentHTML += contentFood;
    }
    document.getElementById("table-food").innerHTML = contentHTML;
};


import { monAnController } from "./controller/monAnController.js";
import { servicesMonAn } from "./services/servicesMonAn.js";
import { spinnersLoading } from "./services/spinnersLoading.js";


let renderservices = () => {
    spinnersLoading.batLoading();
    servicesMonAn
        .layDanhSachMonAn()
        .then((res) => {
            spinnersLoading.tatLoading();
            // 
            listFood = res.data;

            renderListFood(listFood);
        })
        .catch((err) => {
            spinnersLoading.tatLoading();

        });
};
renderservices();

let xoaMonAn = (idMonAn) => {
    servicesMonAn.xoaMonAn(idMonAn)
        .then((res) => {

            renderservices();
        })
        .catch((err) => {

        });
};
window.xoaMonAn = xoaMonAn;

let themMonAn = () => {
    let monAn = monAnController.layThongTinMonAnTuForm();
    spinnersLoading.batLoading();
    servicesMonAn.themMoiMonAn(monAn)
        .then((res) => {

            spinnersLoading.tatLoading();
            renderservices();
        }).catch((err) => {
            spinnersLoading.tatLoading();

        });
};
window.themMonAn = themMonAn;

let suaMonAn = (idMonAn) => {
    spinnersLoading.batLoading();
    idFoodEdited = idMonAn;
    servicesMonAn.layChiTietMonAn(idMonAn).then((res) => {
        spinnersLoading.tatLoading();
        monAnController.showThongTinlenForm(res.data);
    }).catch((err) => {
        spinnersLoading.tatLoading();
    });
};
window.suaMonAn = suaMonAn;

let capNhatMonAn = () => {
    let monAn = monAnController.layThongTinMonAnTuForm();
    spinnersLoading.batLoading();
    let newMonAn = { ...monAn, id: idFoodEdited };
    servicesMonAn.capNhatMon(newMonAn)
        .then((res) => {
            spinnersLoading.tatLoading();
            monAnController.showThongTinlenForm({
                name: "",
                price: "",
                description: ""
            });
            renderservices();
        })
        .catch((err) => {
            spinnersLoading.tatLoading();
        });
};
window.capNhatMonAn = capNhatMonAn;